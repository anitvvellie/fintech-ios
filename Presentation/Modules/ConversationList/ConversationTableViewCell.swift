//
//  ConversationTableViewCell.swift
//  TinkoffChat
//
//  Created by Alevtina on 07/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class ConversationTableViewCell: UITableViewCell {
    

    //MARK: Properties
    
    @IBOutlet weak var userIconImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastTimeMessagedLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userIconImageView.layer.cornerRadius = 40
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
