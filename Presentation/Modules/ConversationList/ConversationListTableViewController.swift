//
//  ConversationListTableViewController.swift
//  ChatPlayground
//
//  Created by Alevtina on 11/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CoreData

class ConversationTableViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet var tableView: UITableView!
    
    let serviceManager = CommunicationService()
    let serviceDelegate = ServiceDelegate()
    
    enum TableSection: Int {
        case online = 0, history, total
    }
    
    var data = [TableSection: [[String: String]]]()
    
    lazy var container: NSPersistentContainer  = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { _,_ in })
        return container
    }()
    
    lazy var fetchResultsUserController: NSFetchedResultsController<User> = {
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: String(describing: User.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.resultType = .managedObjectResultType
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: container.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        controller.delegate = self
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serviceManager.delegate = self.serviceDelegate
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        try? fetchResultsUserController.performFetch()
        
        self.tableView.reloadData()
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "ShowDialog":
                if let cell = sender as? ConversationTableViewCell {
                    if let nextView = segue.destination as? ChatViewController {
                        if let name = cell.userNameLabel.text {
                            //nextView.testText = name
                            nextView.title = name
                            nextView.fromWhom = name
                        }
                    }
                }
                
            default: break
                
            }
        }
    }
    
    func getTimeForPeer(peer: MCPeerID) -> String {
        if let time = self.serviceManager.data[peer] {
            return time
        } else {
            return "never"
        }
    }
    
    private func clearPeerData() {
        self.container.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<User>(entityName: String(describing: User.self))
            do {
                let result = try context.fetch(fetchRequest)
                if result.isEmpty == false {
                    for record in result {
                        context.delete(record)
                    }
                }
            } catch {
                fatalError("Cound not fetch data")
            }
        }
    }
}

// MARK: - UITableViewDelegate

extension ConversationTableViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 44
//    }
}

// MARK: - UITableViewDataSource

extension ConversationTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return 1
        guard let sections = self.fetchResultsUserController.sections else { return 0 }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.serviceManager.foundPeers.count
        guard let sections = self.fetchResultsUserController.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ConversationCellOnline"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ConversationTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let model = self.fetchResultsUserController.object(at: indexPath)
        cell.userNameLabel.text = model.name
        cell.messageLabel.text = model.id
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Online"
        } else {
            return "History"
        }
    }
}


// MARK: - NSFetchedResultsControllerDelegate

extension ConversationTableViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        
        guard let indexPath = indexPath else { return }
        
        switch type {
        case .update:
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else { return }
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            tableView.insertRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}


