//
//  ConversationTableViewCellDelegate.swift
//  TinkoffChat
//
//  Created by Alevtina on 07/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

protocol ConversationCellConfiguration {
    var name: String? { get set }
    var message: String? { get set }
    var date: Date? { get set }
    var isOnline: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}
