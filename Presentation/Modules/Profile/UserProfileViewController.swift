//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Alevtina on 23/09/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var editDataView: UIView!
    @IBOutlet weak var showDataView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userBio: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userBioTextField: UITextField!
    let storage = Storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storage.loadPicture(completion: { image in
            DispatchQueue.main.async {
                self.userImageView.image = image
            }
        })
        
        self.storage.loadName(completion: { name in
            DispatchQueue.main.async {
                self.userName.text = name
                self.userNameTextField.text = name
            }
        })
        
        
        self.storage.loadBio(completion: { bio in
            DispatchQueue.main.async {
                self.userBio.text = bio
                self.userBioTextField.text = bio
            }
        })
        
        self.updateProfileData()
        
        chooseImageButton.imageEdgeInsets = UIEdgeInsets(top:10, left:10, bottom:10, right:10)
        chooseImageButton.layer.cornerRadius = 25
        
        userImageView.layer.cornerRadius = 25
        
        editButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        editButton.layer.borderWidth = 1.5
        editButton.layer.cornerRadius = 10
        
        saveButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        saveButton.layer.borderWidth = 1.5
        saveButton.layer.cornerRadius = 10
        
        editDataView.isHidden = true
        activityIndicator.isHidden = true
    }
    
    @IBAction func editProfileData(_ sender: UIButton) {
        self.editDataView.isHidden = false
        self.showDataView.isHidden = true
    }
    
    @IBAction func saveProfileData(_ sender: UIButton) {
        disableButtons()
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        if let name = self.userNameTextField.text, let bio = self.userBioTextField.text {
            self.storage.saveInfo(name: name, bio: bio, completion: { isDataSaved in
                self.updateProfileData()
            })
        }
        
        self.enableButtons()
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        self.editDataView.isHidden = true
        self.showDataView.isHidden = false
    }
    
    @IBAction func loadNewImage(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Take a photo", style: .default, handler: { (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("Camera is not available")
                self.showErrorAlertWhileAccesingCamera()
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Camera Roll", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Load Picture from Web", style: .default, handler: {_ in
            self.performSegue(withIdentifier: "showPictureSearch", sender: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func showErrorAlertWhileAccesingCamera() {
        let alert = UIAlertController(title: "Error accessing camera", message: "Camera is not available", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        userImageView.image = image
        
        self.storage.savePicture(image: image)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func disableButtons() {
        self.saveButton.isEnabled = false
        self.editButton.isEnabled = false
        self.chooseImageButton.isEnabled = false
        self.userBioTextField.isEnabled = false
        self.userNameTextField.isEnabled = false
    }
    
    private func enableButtons() {
        self.saveButton.isEnabled = true
        self.chooseImageButton.isEnabled = true
        self.userBioTextField.isEnabled = true
        self.userNameTextField.isEnabled = true
        self.editButton.isEnabled = true
    }
    
    private func updateProfileData() {
        self.storage.loadName(completion: { name in
            DispatchQueue.main.async {
                self.userName.text = name
                self.userNameTextField.text = name
            }
        })
        
        self.storage.loadBio(completion: { bio in
            DispatchQueue.main.async {
                self.userBio.text = bio
                self.userBioTextField.text = bio
            }
        })
    }

}

