//
//  DialogTableViewCell.swift
//  TinkoffChat
//
//  Created by Alevtina on 14/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class DialogTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    @IBOutlet weak var messageBubbleImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageBubbleImage.image?.resizableImage(withCapInsets: UIEdgeInsetsMake(17, 30, 17, 30), resizingMode: .stretch )
        messageBubbleImage.image?.withRenderingMode(.alwaysTemplate)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
