//
//  ChatViewController.swift
//  TinkoffChat
//
//  Created by Alevtina on 28/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var chatView: UITableView!
    
    var fromWhom: String?
    var messagesArray: [Dictionary<String, String>] = []
    let serviceManager = CommunicationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatView.delegate = self
        chatView.dataSource = self
        
        messageField.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        if textField.text != nil {
//            let message = Message(identifier: self.serviceManager.generateMessageId(), text: textField.text!)
//            //self.serviceManager.send(message, to: peer)
//        }
//        //let messageDictionary: [String: String] = ["message": textField.text?]
        return true
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
