//
//  MessageCellConfiguration.swift
//  TinkoffChat
//
//  Created by Alevtina on 14/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

protocol MessageCellConfiguration: class {
    var text: String? { get set }
}
