//
//  DialogTableViewController.swift
//  TinkoffChat
//
//  Created by Alevtina on 14/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

let DialogData = [
    ["fromWho": "me", "message": "😘😘😘😘😘😘"],
    ["fromWho": "me", "message": "I’m glad he has Erika- she seems to be an amazing influence for him"],
    ["fromWho": "not me", "message": "I just wanna say I absolutely don’t understand why Shane is getting so much backlash"],
    ["fromWho": "me", "message": "Omgg there's so much shit about Jake and I feel like crying for him he has so much shit to take care of"],
    ["fromWho": "not me", "message": "6 figures I was only 4 ughh that’s stuck in my head now"],
    ["fromWho": "me", "message": "🤯 🤔 🤫"],
    ["fromWho": "not me", "message": "After watching this, I feel like Logan is more of the sociopath and Jake is just a victim of circumstance. Granted, he's done bad shit as well, but Logan is .....I guess worse to me. And in a way, even though I hate both of them, I kind of feel...bad for jake a little bit? I honestly don't know anymore."],
    ["fromWho": "not me", "message": "Next up: Netflix and YouTube start bidding war for Shane’s content﻿"],
    ["fromWho": "me", "message": "I hope that Shane takes time for his mental health after this series. You can tell SO MUCH work was put into every little detail. I’m so incredibly proud and hope he’s celebrating the end of an amazing project.﻿"],
    ["fromWho": "not me", "message": "ok but what do i do now that this is finished?﻿ 🤭"]
]

class DialogTableViewController: UITableViewController {
    
    // MARK: Properties
    
    var fromWhom: String?
    
    enum TableSection: Int {
        case sent = 0, recieved, total
    }
    
    var data = [TableSection: [[String: String]]]()
    
    // MARK: Private functions
    
    private func sortData() {
        data[.sent] = DialogData.filter({ $0["fromWho"] == "me" })
        data[.recieved] = DialogData.filter({ $0["fromWho"] == "not me" })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sortData()
        print("Dialog with \(self.fromWhom!)")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return TableSection.total.rawValue
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tableSection = TableSection(rawValue: section), let dialogData = data[tableSection] {
            return dialogData.count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let tableSection = TableSection(rawValue: indexPath.section), let message = data[tableSection]?[indexPath.row] {
            switch tableSection {
            case .recieved:
                let cellIdentifier = "IncomingMessage"
                guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DialogTableViewCell
                    else {
                        fatalError("The dequeued cell is not an instance of DialogTableViewCell.")
                }
                cell.messageLabel.text = message["message"]
                return cell
            case .sent:
                let cellIdentifier = "OutgoingMessage"
                guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? DialogTableViewCell
                    else {
                        fatalError("The dequeued cell is not an instance of DialogTableViewCell.")
                }
                cell.messageLabel.text = message["message"]
                return cell
            case .total:
                return UITableViewCell()
            }
        }
        return UITableViewCell()
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
