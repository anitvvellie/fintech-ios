//
//  PictureSearchCollectionViewCell.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class PictureSearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
