//
//  PictureSearchCollectionViewController.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import UIKit

class PictureSearchCollectionViewController: UICollectionViewController {
    
    fileprivate let reuseIdentifier = "FlickrCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    fileprivate let pixabayService = PixabayService()
    fileprivate var pictures: [Picture]?
    fileprivate let itemsPerRow: CGFloat = 3
    
    let storageService = Storage()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadPictures()
        
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
}
// MARK: - UICollectionViewDataSource
extension PictureSearchCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pictures = pictures else { return 0 }
        return pictures.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let pictures = pictures else { return UICollectionViewCell() }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FlickrCell", for: indexPath) as? PictureSearchCollectionViewCell else {
            return UICollectionViewCell()
        }

        let picture = pictures[indexPath.row]
        picture.returnPreviewPicture(completion: { image in
            DispatchQueue.main.async {
                cell.imageView.image = image
            }
        })

        return cell
        
    }
}

// MARK: -UICollectionViewDelegateFlowLayout
extension PictureSearchCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
}

// MARK: -UICollectionViewDelegate
extension PictureSearchCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let pictures = pictures else {
            print("Pictures are not accesible")
            return
        }
        let picture = pictures[indexPath.row]
        picture.returnLargePicture(completion: { image in
            if let image = image {
                self.storageService.savePicture(image: image)
                print("Picture was saved! ")
                //self.performSegue(withIdentifier: "showUserProfile", sender: nil)
            } else {
                print("There is no image :(")
            }
        })
        
    }
}

// MARK: - Private
extension PictureSearchCollectionViewController {
    
    func loadPictures() {
        DispatchQueue.global().async {
            self.pixabayService.requestPicture(completion: { [weak self] (pictures, error) in
                if let error = error {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: error.localizedDescription,
                                                      message: nil,
                                                      preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self?.present(alert, animated: true, completion: nil)
                    }
                } else if let pictures = pictures {
                    self?.pictures = pictures
                    DispatchQueue.main.async {
                        self?.collectionView?.reloadData()
                    }
                }
            })
        }
    }
    
}




