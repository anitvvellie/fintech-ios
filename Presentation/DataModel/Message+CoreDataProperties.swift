//
//  Message+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Alevtina on 11/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//
//

import Foundation
import CoreData


extension Message {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Message> {
        return NSFetchRequest<Message>(entityName: "Message")
    }

    @NSManaged public var fromUserId: String?
    @NSManaged public var identifier: String?
    @NSManaged public var text: String?
    @NSManaged public var toUserId: String?
    @NSManaged public var timeSent: NSDate?
    @NSManaged public var converstionId: String?
    @NSManaged public var conversation: Conversation?
    @NSManaged public var whoTexted: User?

}
