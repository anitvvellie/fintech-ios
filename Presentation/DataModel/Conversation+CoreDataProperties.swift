//
//  Conversation+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Alevtina on 11/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//
//

import Foundation
import CoreData


extension Conversation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Conversation> {
        return NSFetchRequest<Conversation>(entityName: "Conversation")
    }

    @NSManaged public var date: String?
    @NSManaged public var hasUnreadMessages: Bool
    @NSManaged public var message: String?
    @NSManaged public var id: String?
    @NSManaged public var lastMessage: Message?
    @NSManaged public var user: User?

}
