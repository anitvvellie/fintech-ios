//
//  User+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Alevtina on 04/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var bio: String?
    @NSManaged public var name: String?
    @NSManaged public var picturePath: String?

}
