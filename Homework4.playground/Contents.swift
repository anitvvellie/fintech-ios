//: Playground - noun: a place where people can play

import UIKit


class Company {
    
    var name: String
    var productManager: Manager?
    var ceo: CEO?
    
    init(name: String) {
        self.name = name
        print("Company has been initialized")
    }
    deinit {
        print("☠️ Company has been deinitialized ☠️")
    }
    
    func setCEO() {
        let ceo = CEO()
        self.ceo = ceo
    }
    
    func hireManager(name: String) {
        let manager = Manager(name: name, boss: ceo!)
        self.productManager = manager
    }
    
    func createCompany(nameOfTheCompany: String) -> Company {
        let comp = Company(name: nameOfTheCompany)
        comp.setCEO()
        comp.hireManager(name: "HereCouldBeTheNameOfYourManager")
        comp.productManager!.hireNewDeveloper(name: "HereCouldBeTheNameOfYourDeveloper1")
        comp.productManager!.hireNewDeveloper(name: "HereCouldBeTheNameOfYourDeveloper2")
        return comp
    }
}

class CEO {
    

    weak var manager: Manager?
    
    lazy var myDevelopers: () -> Void = {
        [unowned self] in
        var mydevs = "My devs are: "
        
        if self.manager != nil {
            for dev in self.manager!.developers {
                mydevs.append(dev.name + ", ")
            }
            if (self.manager!.developers.count == 0) {
                mydevs = "I have no developers yet"
            }
        }

        print(mydevs)
    }
    
    lazy var myCompany: () -> Void = {
        [unowned self] in
        var myComp = "My company is : ME"
        
        if self.manager != nil {
            myComp.append(", my manager \(self.manager!.name)")
            for dev in self.manager!.developers {
                myComp.append(dev.name + ", ")
            }
            if (self.manager!.developers.count == 0) {
                myComp = "I have no developers yet"
            }
        }
        print(myComp)
    }
    
    lazy var myProductManager: () -> Void = {
        [unowned self] in
        var myManager = "My manager is "
        if self.manager != nil {
            myManager.append(self.manager!.name)
        }
        print(myManager)
    }
    
    init() {
        print("The CEO has been initialized")
    }
    
    func getNewMessage(from manager: Manager, about developer: Developer, message: String) {
        print("CEO got new message from \(developer.name) via \(manager.name): \(message)")
    }
    
    deinit {
        print("☠️ The CEO is dead ☠️")
    }

}

class Manager {
    
    var developers = [Developer]()
    var boss: CEO
    var name: String
    
    init(name: String, boss: CEO) {
        self.name = name
        self.boss = boss
        print("Manager has been initialized")
    }
    
    func hireNewDeveloper(name: String) {
        let dev = Developer(name: name, manager: self)
        developers.append(dev)
        print("New developer \(name) was hired by manager")
    }
    
    func fireDeveloper(name: String) {
        if let index = developers.index(where: { $0.name == name }) {
            developers.remove(at: index)
            print("Developer \(name) was fired")
        }
    }
    
    func giveNewTask(to developer: Developer) -> String {
        return "do smt special surprise me"
    }
    
    func sendMessageToCEO(from developer: Developer, message: String) {
        self.boss.getNewMessage(from: self, about: developer, message: message)
    }
    func sendMessageToDeveloper(to name: String, from developer: Developer, message: String) {
        for dev in developers {
            if dev.name == name {
                dev.getMessage(from: developer.name, message: message)
            }
        }
    }
    
    deinit {
        print("☠️ The manager is dead ☠️")
    }
}

class Developer {
    
    var manager: Manager
    var name: String
    var tasks = [String]()
    
    init(name: String, manager: Manager) {
        self.name = name
        self.manager = manager
        print("Developer \(name) has been initialized")
    }
    
    func askManagerToGetNewTask() {
        let newTask = self.manager.giveNewTask(to: self)
        tasks.append(newTask)
    }
    
    func sendMessage(to developer: String, message: String) {
        print("\(name) says to \(developer): \(message)")
        self.manager.sendMessageToDeveloper(to: developer, from: self, message: message)
    }
    
    func getMessage(from developer: String, message: String) {
        print("\(name) got new message from \(developer) : \(message)")
    }
    
    func saySomethingToCEO(message: String) {
        self.manager.sendMessageToCEO(from: self, message: message)
    }
    
    deinit {
        print("☠️ Developer \(name) is dead ☠️")
    }
}


//var boss: CEO
//var manager: Manager?
//var dev1: Developer?
//var dev2: Developer?
//var dev3: Developer?
//
//boss = CEO()
//manager = Manager(name: "Mr. Manager", boss: boss)
//dev1 = Developer(name: "Bob", manager: manager!)
//dev2 = Developer(name: "Kate", manager: manager!)
//dev3 = Developer(name: "Michael", manager: manager!)
//
//boss.manager = manager
//manager!.developers += [dev1!, dev2!, dev3!]
////dev1!.askManagerToGetNewTask()
////dev1!.sendMessage(to: "Kate", message: "wassup")
//manager!.developers = []


var comp: Company?
comp = Company(name: "Best Company Ever")
comp!.setCEO()
comp!.hireManager(name: "Mr. Robot")
comp!.productManager!.hireNewDeveloper(name: "Bob")
comp!.productManager!.hireNewDeveloper(name: "Alice")
comp!.productManager!.hireNewDeveloper(name: "Mike")
comp!.productManager!.developers[0].askManagerToGetNewTask()
comp!.productManager!.developers[1].saySomethingToCEO(message: "Hey I need more money")
comp!.productManager!.developers[0].sendMessage(to: "Alice", message: "wassup")
comp!.ceo!.myCompany
comp!.ceo!.myProductManager
comp!.productManager!.fireDeveloper(name: "Bob")
comp!.productManager!.fireDeveloper(name: "Alice")
comp!.productManager!.fireDeveloper(name: "Mike")

comp = nil








