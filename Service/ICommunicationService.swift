//
//  ICommunicationService.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    
    // Service delegate
    var delegate: CommunicationServiceDelegate? { get set }
    var online: Bool { get set }
    
    // send message to the peer
    func send(_ message: MessageStruct, to peer: Peer)
    
}
