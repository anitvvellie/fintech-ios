//
//  PixabayService.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

private let apiKey = "10799629-a63876eb3e5b599c4a7945cc7"
private let urlString = "https://pixabay.com/api/?key=\(apiKey)&q=yellow+flowers&image_type=photo&pretty=true&per_page=50"

protocol IPixabayService {
    
    func requestPicture(completion: @escaping ([Picture]?, Error?) -> Void)
    
}

final class PixabayService: IPixabayService {
    
    func requestPicture(completion: @escaping ([Picture]?, Error?) -> Void) {
        
        guard let url = URL(string: urlString) else {
            fatalError()
        }
        
        let searchRequest = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: searchRequest, completionHandler: { (data, response, error) in
            if let _ = error {
                let APIError = NSError(domain: "PictureSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                OperationQueue.main.addOperation({
                    completion(nil, APIError)
                })
                return
            }
            
            guard let _ = response as? HTTPURLResponse, let data = data else {
                let APIError = NSError(domain: "PictureSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                OperationQueue.main.addOperation({
                    completion(nil, APIError)
                })
                return
            }
            do {
                let pixabayResponse = try JSONDecoder().decode(PixabayResponse.self, from: data)
                completion(pixabayResponse.hits, nil)
            } catch {
                fatalError("Could not decode Pixabay response")
            }
        }) .resume()
    }
}

