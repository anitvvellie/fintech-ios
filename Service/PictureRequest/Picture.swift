//
//  Picture.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import UIKit

struct Picture: Codable {
    
    let previewURL: String?
    let largeImageURL: String?
    

    func returnPreviewPicture(completion: @escaping ((UIImage?) -> Void)) {
        guard let previewURL = self.previewURL else {
            completion(nil)
            return
        }
        if let url = URL(string: previewURL) {
            do {
                let imageData = try Data(contentsOf: url)
                if let image = UIImage(data: imageData) {
                    completion(image)
                }
            } catch {
                fatalError("Could not get URL")
            }
        }
    }
    
    func returnLargePicture(completion: @escaping ((UIImage?) -> Void)) {
        guard let largeURL = self.largeImageURL else {
            completion(nil)
            return
        }
        if let url = URL(string: largeURL) {
            do {
                let imageData = try Data(contentsOf: url)
                if let image = UIImage(data: imageData) {
                    completion(image)
                }
            } catch {
                fatalError("Could not get URL")
            }
        }
    }
    
}

struct PixabayResponse: Codable {
    
    let hits: [Picture]
    
}
