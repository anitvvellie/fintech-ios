//
//  Peer.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

struct Peer {
    
    let identifier: String
    let name: String
    
}
