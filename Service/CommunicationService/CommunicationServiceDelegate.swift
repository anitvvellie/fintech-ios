//
//  CommunicationDelegate.swift
//  TinkoffChat
//
//  Created by Alevtina on 18/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

protocol CommunicationServiceDelegate: class {
    
    /// Browsing
    func communicationService(_ communicationService: ICommunicationService,
                              didFoundPeer peer: Peer)
    func communicationService(_ communicationService: ICommunicationService,
                              didLostPeer peer: Peer)
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartBrowsingForPeers error: Error)
    /// Advertising
    func communicationService(_ communicationService: ICommunicationService,
                              didReceiveInviteFromPeer peer: Peer,
                              invintationClosure: (Bool) -> Void)
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartAdvertisingForPeers error: Error)
    /// Messages
    func communicationService(_ communicationService: ICommunicationService,
                              didReceiveMessage message: MessageStruct,
                              from peer: Peer)
    
    func connectedDevicesChanged(manager: CommunicationService, connectedDevices: [String])
    
}
