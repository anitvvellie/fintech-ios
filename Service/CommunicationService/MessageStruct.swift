//
//  Message.swift
//  TinkoffChat
//
//  Created by Alevtina on 25/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

struct MessageStruct {
    enum `Type`: String {
        case textMessage = "TextMessage"
    }
    
    let identifier: String
    let text: String
}
