//
//  ServiceDelegate.swift
//  TinkoffChat
//
//  Created by Alevtina on 19/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import CoreData

class ServiceDelegate: CommunicationServiceDelegate {
    
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: {_,_ in })
        return container
    }()

    
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        self.container.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<User>(entityName: String(describing: User.self))
            fetchRequest.predicate = NSPredicate(format: "id==%@", peer.identifier)
            if let result = try? context.fetch(fetchRequest) {
                if result.isEmpty {
                    let user = NSEntityDescription.insertNewObject(forEntityName: String(describing: User.self), into: context)
                        as? User
                    user?.name = peer.name
                    user?.id = peer.identifier
                    user?.bio = "test bio"
                    
                    do {
                        try context.save()
                        print("✅ Data was saved via CommunicationServerDelegate")
                    } catch {
                        print("Could not save data")
                    }
                } else {
                    print("❌ Found user is already in coreData")
                    return
                }
            }
        }
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        print("Delagate lost a peer")
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {}
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: (Bool) -> Void) {}
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {}
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: MessageStruct, from peer: Peer) {}
    
    func connectedDevicesChanged(manager: CommunicationService, connectedDevices: [String]) {
         print("Number of connected devices changed")
    }
    
    
    
    
}
