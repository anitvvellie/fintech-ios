//
//  MPCManager.swift
//  TinkoffChat
//
//  Created by Alevtina on 28/10/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CommunicationService: NSObject, ICommunicationService {
    
    var delegate: CommunicationServiceDelegate?
    var online: Bool
    var session: MCSession!
    var browser: MCNearbyServiceBrowser!
    var advertiser: MCNearbyServiceAdvertiser!
    var peer: MCPeerID!
    var foundPeers = [MCPeerID]()
    var data = [ MCPeerID : String ]()
    let myIdentifier = UIDevice.current.identifierForVendor?.uuidString ?? "default"
    
    var invitationHandler: ((Bool, MCSession?)->Void)?
    
    let serviceType = "tinkoff-chat"
    
    override init() {
        
        peer = MCPeerID(displayName: UIDevice.current.name)
        
        session = MCSession(peer: self.peer)
        browser = MCNearbyServiceBrowser(peer: self.peer, serviceType: serviceType)
        advertiser = MCNearbyServiceAdvertiser(peer: self.peer, discoveryInfo: ["username" : self.peer.displayName, "identifier": self.myIdentifier], serviceType: serviceType)
        online = true
        
        super.init()
        
        session.delegate = self
        browser.delegate = self
        advertiser.delegate = self
        
        advertiser.startAdvertisingPeer()
        browser.startBrowsingForPeers()
        
        print("Peer: \(self.peer)")
        
    }
    
    func send(_ message: MessageStruct, to peer: Peer) {
        let eventType = "TextMessage"
        let messageId = message.identifier
        let text = message.text
        var mcPeer: MCPeerID
        if let index = foundPeers.index(where: {$0.displayName == peer.name}) {
            mcPeer = foundPeers[index]
        } else {
            return
        }
        
        let jsonObject: [String : Any] = [
            "eventType" : eventType,
            "messageId" : messageId,
            "text" : text
        ]
    
        
        do {
            let data = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
            try self.session.send(data, toPeers: [mcPeer], with: .reliable)
        } catch {
            print("Could not serialise JSON 😥")
            print(error.localizedDescription)
        }

        
    }
    
    func generateMessageId() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }
    
    func getCurrentTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .hour, .minute], from: date)
        
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minutes = components.minute
        
        return "\(hour ?? 00):\(minutes ?? 00 ) \(day ?? 00).\(month ?? 00)"
    }
    
}

extension CommunicationService: MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        foundPeers.append(peerID)
        if let discoveryInfo = info {
            if let id = discoveryInfo["identifier"], let name = discoveryInfo["username"] {
                let myPeer = Peer(identifier: id, name: name)
                delegate?.communicationService(self, didFoundPeer: myPeer)
                browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 10)
                print("❗️Found new peer with name: \(name), id: \(id)")
            }
        } else {
            print("⚠️ Could not get discovery info of the found peer")
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        if let index = foundPeers.index(where: {$0 == peerID}) {
            foundPeers.remove(at: index)
            
            let myPeer = Peer(identifier: "\(peerID)", name: peerID.displayName)
            delegate?.communicationService(self, didLostPeer: myPeer)
            
 //           print("🚫Lost peer \(peerID)")
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print(error.localizedDescription)
        
        delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }
    
}

extension CommunicationService: MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
//        print("📩 Did recieve invitation from \(peerID)")
        invitationHandler(true, self.session)
        
        //let myPeer = Peer(identifier: "\(peerID)", name: peerID.displayName)
        //delegate?.communicationService(self, didReceiveInviteFromPeer: myPeer, invintationClosure: )
        
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print(error.localizedDescription)
        
        delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }
    
}

extension CommunicationService: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case MCSessionState.connected:
            data.updateValue(getCurrentTime(), forKey: peerID)
            
        case MCSessionState.connecting:
            break
            
        case MCSessionState.notConnected:
            if let index = data.index(forKey: peerID) {
                data.remove(at: index)
            }
        }
        self.delegate?.connectedDevicesChanged(manager: self, connectedDevices: self.session.connectedPeers.map{$0.displayName})
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
}
