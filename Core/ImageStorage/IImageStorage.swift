//
//  IImageStorage.swift
//  TinkoffChat
//
//  Created by Alevtina on 18/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import UIKit

protocol IImageStorage {
    
    func savePicture(image: UIImage)
    func loadPicture(completion: @escaping ((UIImage?) -> Void))
    
}
