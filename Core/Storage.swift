//
//  Storage.swift
//  TinkoffChat
//
//  Created by Alevtina on 04/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Storage {
    
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: {_,_ in })
        return container
    }()
    
}

extension Storage: IImageStorage {
    func savePicture(image: UIImage) {
        let imageFileName = "userPicture.jpg"
        let imageData = UIImageJPEGRepresentation(image, 0)
        
        if let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let imageURL = documentDirectory.appendingPathComponent(imageFileName)
            do {
                try imageData?.write(to: imageURL)
            } catch {
                fatalError("Could not write the image to the file: \(error.localizedDescription)")
            }
        }
        
        self.container.performBackgroundTask { (backgroundContext) in
            let request = NSFetchRequest<User>(entityName: String(describing: User.self))
            let result = try? backgroundContext.fetch(request)
            
            if result?.first != nil {
                let user = result?.first
                user?.picturePath = imageFileName
            } else {
                let user = NSEntityDescription.insertNewObject(forEntityName: String(describing: User.self), into: backgroundContext) as? User
                user?.picturePath = imageFileName
            }
            do {
                try backgroundContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            print("Data was saved")
        }
    }
    
    func loadPicture(completion: @escaping ((UIImage?) -> Void)) {
        self.container.performBackgroundTask { (backgroundContext) in
            let request = NSFetchRequest<User>(entityName: String(describing: User.self))
            let result = try? backgroundContext.fetch(request)
            let user = result?.first
            
            if let imageFilePath = user?.picturePath {
                if let documentDirectory = FileManager.default.urls(for: .documentDirectory,  in: .userDomainMask).first {
                    let imageURL = documentDirectory.appendingPathComponent(imageFilePath)
                    if let data = try? Data(contentsOf: imageURL),
                        let image = UIImage(data: data) {
                        completion(image)
                    } else {
                        print("Could not get image out of data")
                    }
                } else {
                    print("Could not get data from URL")
                }
            } else {
                print("User has no image yet :(")
            }
        }
    }
}

extension Storage: IUserInfoStorage {
    func loadName(completion: @escaping ((String) -> Void)) {
        var name = "Default"
        self.container.performBackgroundTask { (backgroundContext) in
            let request = NSFetchRequest<User>(entityName: String(describing: User.self))
            let result = try? backgroundContext.fetch(request)
            let user = result?.first
            
            if user?.name != nil {
                name = (user?.name)!
                print("User's name is \(name)")
                completion(name)
            }
        }
    }
    
    func loadBio(completion: @escaping ((String) -> Void)) {
        var bio = "Default"
        self.container.performBackgroundTask { (backgroundContext) in
            let request = NSFetchRequest<User>(entityName: String(describing: User.self))
            let result = try? backgroundContext.fetch(request)
            let user = result?.first
            
            if user?.bio != nil {
                bio = (user?.bio)!
                print("User's bio is \(bio)")
                completion(bio)
            }
        }
    }
    
    func saveInfo(name: String, bio: String, completion: @escaping ((Bool) -> Void)) {
        self.container.performBackgroundTask { (backgroundContext) in
            let request = NSFetchRequest<User>(entityName: String(describing: User.self))
            let result = try? backgroundContext.fetch(request)
            
            if result?.first != nil {
                let user = result?.first
                user?.name = name
                user?.bio = bio
                
            } else {
                let user = NSEntityDescription.insertNewObject(forEntityName: String(describing: User.self), into: backgroundContext) as? User
                user?.name = name
                user?.bio = bio
            }
            
            do {
                try backgroundContext.save()
                completion(true)
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            print("Data was saved")
        }
    }
}

