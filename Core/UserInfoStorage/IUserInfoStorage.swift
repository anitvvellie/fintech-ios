//
//  IUserInfoStorage.swift
//  TinkoffChat
//
//  Created by Alevtina on 18/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation

protocol IUserInfoStorage {
    
    func loadName(completion: @escaping ((String) -> Void))
    func loadBio(completion: @escaping ((String) -> Void))
    func saveInfo(name: String, bio: String, completion: @escaping ((Bool) -> Void))
    
}
