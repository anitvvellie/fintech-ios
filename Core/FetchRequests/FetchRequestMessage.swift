//
//  FetchRequestMessage.swift
//  TinkoffChat
//
//  Created by Alevtina on 11/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import CoreData

class FetchRequestMessage {
    
    func getRequestForShowingMessageFrom(conversation id: String) -> NSFetchRequest<Message> {
        let sortDescriptor = NSSortDescriptor(key: "timeSent", ascending: false)
        let fetchRequest = NSFetchRequest<Message>(entityName: String(describing: Message.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "message.conversationId == %@", id)
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
}
