//
//  FetchRequestConversation.swift
//  TinkoffChat
//
//  Created by Alevtina on 11/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import CoreData

class FetchRequestConversation {
    
    func getRequestForShowingConversation(by id: String) -> NSFetchRequest<Conversation> {
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "conversation.id == %@", id)
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
    func getRequestForShowingNotEmptyConversationOnline(conversation id: String) -> NSFetchRequest<Conversation> {
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        let fetchRequest = NSFetchRequest<Conversation>(entityName: String(describing: Conversation.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "conversation.user.isOnline == true && conversation.lastMessage != nil")
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
}
