//
//  FetchResultUser.swift
//  TinkoffChat
//
//  Created by Alevtina on 11/11/2018.
//  Copyright © 2018 Alevtina. All rights reserved.
//

import Foundation
import CoreData

class FetchRequestUser {
    
    func getRequestForShowingUser(by id: String) -> NSFetchRequest<User> {
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: String(describing: User.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "user.id == %@", id)
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
    func getRequestForShowingOnlineUsers() -> NSFetchRequest<User> {
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: String(describing: User.self))
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format: "user.isOnline == true")
        fetchRequest.resultType = .managedObjectResultType
        
        return fetchRequest
    }
    
}


